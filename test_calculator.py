import calculator


class TestCalculator:

    def test_addition(self):
        assert 10 == calculator.add(4, 6)

    def test_multiplication(self):
        assert 15 == calculator.multiply(3, 5)
